import {Logger as winston} from "winston";
import { Logger } from "../common/types";

export class WinstonLogger implements Logger {
	private logger: winston;

	constructor(logger: winston) {
		if (!logger) {
			throw Error("Logger cannot be undefined");
    };
    this.logger = logger;
	};

	info(text: string): void {
		this.logger.info(text);
  };

  debug(text: string): void {
    this.logger.debug(text);
  };
  
	warning(text: string): void {
    this.logger.warn(text);
  };

  error(text: string): void {
    this.logger.error(text);
  };
};
import { Intent } from "../../../src/common/types";

export abstract class IntentBase<T> implements Intent<T> {

  /**
   * Intent logic
   * @param conv 
   */
  abstract invoke(conv: T): T;

};
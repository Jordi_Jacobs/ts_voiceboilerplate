import { GoogleIntent } from "./googleIntent";
import { DialogflowConversation } from "actions-on-google";

export class WelcomeIntent extends GoogleIntent {
  
  invoke(conv: DialogflowConversation): DialogflowConversation {
    return conv.close("Hello World");
  };
};
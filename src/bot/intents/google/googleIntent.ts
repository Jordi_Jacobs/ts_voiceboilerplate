import { DialogflowConversation } from "actions-on-google";
import WinstonLoggerBuilder from"../../../helpers/builders/winstonLoggerBuilder";
import { IntentBase } from "../intentBase";
import { Logger } from "../../../common/types";

/**
 * Abstract class for any Google Assistant / Dialogflow Intents
 */
export abstract class GoogleIntent implements IntentBase<DialogflowConversation> {

  protected logger: Logger

  constructor() {
    const loggerLabel = this.getClassName();
    this.logger = WinstonLoggerBuilder.createWinstonLogger(loggerLabel);
  };

  abstract invoke(conv: DialogflowConversation): DialogflowConversation

  /**
   * Get the class name from the constructor 
   * and adds a .ts extension to the returned string.
   * @returns {string} class name with .ts appended to it.
   */
  private getClassName(): string {
    const className = `Google | ${this.constructor.name}.ts`;
    return className;
  };
};
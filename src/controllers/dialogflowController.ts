import { DialogflowConversation, dialogflow } from "actions-on-google";
import { IntentNames, IDialogflowEndpoint } from "../common/types";
import { WelcomeIntent } from "../../src/bot/intents/google/welcomeIntent";

export class DialogflowController {
  public endpoint: IDialogflowEndpoint;

  constructor() {
    this.endpoint = dialogflow();
    this.setupIntentLogic();
  };

  private setupIntentLogic() {
    this.endpoint.intent(IntentNames.welcomeIntent, (conv: DialogflowConversation) => {
      return new WelcomeIntent().invoke(conv);
    });
  };
};
require('dotenv').config()
import express from "express";
import { DialogflowController } from "./controllers/dialogflowController";

const app = express();

app.use(express.json());

const dialogflowController = new DialogflowController();

app.post("/dialogflow", dialogflowController.endpoint);

app.listen(process.env.PORT, () => {
  console.log(`Express Server listening on port: ${process.env.PORT}`)
});
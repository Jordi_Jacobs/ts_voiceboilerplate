import { OmniHandler, BaseApp, DialogflowApp, Contexts, DialogflowConversation } from "actions-on-google";

// Interfaces

export interface Logger {
  info(text: string): void;
  debug(text: string): void;
  warning(text: string): void;
  error(text: string): void;
};

export interface Intent<T> {
  invoke(conv: T): T;
};

export interface IDialogflowEndpoint extends OmniHandler, BaseApp, 
  DialogflowApp<{}, {}, Contexts, DialogflowConversation<{}, {}, Contexts>> {};

// Enums
export const enum IntentNames {
  welcomeIntent = "Default Welcome Intent"
};
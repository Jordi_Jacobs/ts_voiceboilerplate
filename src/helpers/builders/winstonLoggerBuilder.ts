import winston from "winston";
import { Logger } from "../../common/types";
import { WinstonLogger } from "../../loggers/winstonLogger";
const {combine, timestamp, printf, colorize} = winston.format;

const minimumShownLogLevel = process.env.MinimumLoggerLevel;

class WinstonLoggerBuilder {

    public createWinstonLogger(className: string): Logger{
      if (className === null) {
        throw Error("className cannot be null");
      };

      const loggingFormat = printf(({timestamp, level, message}) => {
        return `[${timestamp}:${level}] ${className}: ${message}`;
      });
      const logger = winston.createLogger({
        format: combine(
          timestamp(),
          colorize(),
          loggingFormat
        ),
        level: minimumShownLogLevel,
        transports: [
          new winston.transports.Console()
        ]
      });

      const loggerWrapper = new WinstonLogger(logger);

      return loggerWrapper;
    };
};

export = new WinstonLoggerBuilder();